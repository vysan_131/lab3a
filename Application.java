public class Application {
	public static void main(String[] args) {
		Student firstYear = new Student();
		firstYear.sport = "football";
		firstYear.lunch ="sandwich";
		firstYear.age = 17;
		System.out.println(firstYear.sport + " " + firstYear.lunch + " " + firstYear.age);
		
		Student secondYear = new Student();
		secondYear.sport = "soccer";
		secondYear.lunch ="pizza";
		secondYear.age = 18;
		System.out.println(secondYear.sport + " " + secondYear.lunch + " " + secondYear.age);
		
		firstYear.sayTeam();
		firstYear.sayAge();
		secondYear.sayTeam();
		secondYear.sayAge();
		
		Student[] section4 = new Student[3];
		section4[0] = firstYear;
		section4[1] = secondYear;
		System.out.println(section4[0].sport);
		section4[2] = new Student();
		System.out.println(section4[2].sport);
		System.out.println(section4[2].lunch);
		System.out.println(section4[2].age);
	}
}	